// @ts-check
/**
 * @param {number[]} candidates
 * @param {number} target
 * @return {number[][]}
 */
var combinationSum = function (candidates, target) {
    const arr = []
    const set = new Set()
    /**
    * @param {number[]} candidates
    * @param {number} target
    * @param {number[]} result
    */
    const loop = function loop(candidates, target, result) {
        if (target < 0 || candidates.length === 0) {
            return
        }
        for (let i = 0; i < candidates.length; i++) {
            const n = candidates[i]
            if (target % n === 0) {
                let _t = result.concat(Array(target / n).fill(n)).sort((a, b) => a - b)
                let key = _t.join(',')
                if (!set.has(key)) {
                    set.add(key)
                    arr.push(_t)
                }
            }
            loop(candidates.slice(0, i).concat(candidates.slice(i + 1)), target - n, result.concat(n))
        }
    }

    loop(candidates, target, [])

    return arr
};