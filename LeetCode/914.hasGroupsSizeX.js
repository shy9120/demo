/**
 * @param {number[]} deck
 * @return {boolean}
 */
var hasGroupsSizeX = function (deck) {
    let m = {}
    for (let i = 0; i < deck.length; i++) {
        let c = deck[i]
        m[c] = m[c] || 0
        m[c] += 1
    }
    var keys = Object.keys(m)
    for (let i = 0; i < keys.length; i++) {
        if (m[keys[i]] === 1) return false
        for (let j = i + 1; j < keys.length; j++) {
            let a = m[keys[i]]
            let b = m[keys[j]]
            if (a < b) {
                a = b
                b = m[keys[i]]
            }
            while (a > 1 && b > 1) {
                let t = a % b;
                a = b
                b = t
            }
            if (b === 1) {
                return false
            }
        }
    }
    return true
};

