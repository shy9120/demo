/**
 * @param {string} seq
 * @return {number[]}
 */
var maxDepthAfterSplit = function (seq) {
    let dep = 0
    return [...seq].map(item => item === '(' ? dep++ % 2 : --dep % 2)
};