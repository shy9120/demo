/**
 * @param {number[][]} board
 * @return {void} Do not return anything, modify board in-place instead.
 */
var gameOfLife = function (board) {
    let res = []
    for (let i = 0; i < board.length; i++) {
        let arr = []
        res.push(arr)
        for (let j = 0; j < board[i].length; j++) {
            arr[j] = ~~getLive(board, i, j)
        }
    }
    res.forEach((item, i) => board[i] = item)
    return board
};
var getLive = function (board, i, j) {
    var sum = ~~board[i][j - 1] + ~~board[i][j + 1]
    if (i > 0) {
        sum += board[i - 1][j] + ~~board[i - 1][j - 1] + ~~board[i - 1][j + 1]
    }
    if (sum > 3) {
        return 0
    }
    if (i < board.length - 1) {
        sum += board[i + 1][j] + ~~board[i + 1][j - 1] + ~~board[i + 1][j + 1]
    }
    if (sum === 3 || board[i][j] && sum === 2) {
        return 1
    } else {
        return 0
    }
};

console.log(
    gameOfLife([
        [0, 1, 0],
        [0, 0, 1],
        [1, 1, 1],
        [0, 0, 0]
    ])
)