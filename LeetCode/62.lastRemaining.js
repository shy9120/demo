/**
 * @param {number} n
 * @param {number} m
 * @return {number}
 */
var lastRemaining = function (n, m) {
    // 约瑟夫环
    let base = { value: 0 }
    let t = base
    for (let i = 1; i < n; i++) {
        t.next = { value: i }
        t = t.next
    }
    t.next = base // 闭环 over

    let sum = n
    let prev
    while(sum > 1) {
        const  f = (m % sum) + sum;
        for (let i = 0; i < m; i++) {
            prev = t
            t = t.next
        }
        prev.next = t.next
        sum--
    }
    return prev.next.value
};

console.log(
    lastRemaining(70866, 116922)
)