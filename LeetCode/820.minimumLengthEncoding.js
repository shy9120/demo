/**
 * @param {string[]} words
 * @return {number}
 */
var minimumLengthEncoding0 = function (words) {
    var same_items = new Set()
    var sub_items = new Set()
    words.sort(function (w1, w2) {
        if (w1 === w2) {
            same_items.add(w1)
            return 0
        }
        let len1 = w1.length
        let len2 = w2.length
        let t1 = '', t2 = '';
        for (let i = 0; i < len1 && i < len2; i++) {
            t1 += w1[len1 - i - 1]
            t2 += w2[len2 - i - 1]
            if (t1 === t2) {
                continue
            } else {
                return t1 > t2 ? 1 : -1
            }
        }
        if (len1 > len2) {
            sub_items.add(w2)
            return 1
        } else {
            sub_items.add(w1)
            return -1
        }
    })
    let used = new Set()
    return words.reduce((n, s) => {
        if (sub_items.has(s) || used.has(s)) {
            return n
        }
        if (same_items.has(s)) {
            used.add(s)
        }
        return n + s.length + 1
    }, 0)
};

/**
 * @param {string[]} words
 * @return {number}
 */
var minimumLengthEncoding = function (words) {
    let pass = ''
    words.sort(function (a, b) {
        return b.length - a.length
    }).forEach(word => {
        let w = word + '#'
        if (!pass.includes(w)) {
            pass += w
        }
    })
    return pass.length
}
console.log(
    minimumLengthEncoding(["time", "me", "bell"])
)