// @ts-check
(async function () {
    const strict = location.search.includes('strict')
    const STORE_KEY = 'chengyucidian'
    const localforage = window['localforage']
    const replacer = (str = '') => strict ? str : str
        .replace(/[āáǎà]/g, 'a')
        .replace(/[ōóǒò]/g, 'o')
        .replace(/[êēéěè]/g, 'e')
        .replace(/[īíǐì]/g, 'i')
        .replace(/[ūúǔù]/g, 'u')
        .replace(/[ǖǘǚǜü]/g, 'v')

    /** @type {string} */
    let db = await localforage.getItem(STORE_KEY)
    if (!db) {
        db = await fetch('./all').then(res => res.text())
        await localforage.setItem(STORE_KEY, db)
    }

    const words = []
    const map = new Map()
    const map_head = new Map()
    db.split(/\n/).map(line => {
        if (line.includes('拼音：')) {
            const [ value, pinyin, desc ] = line.split(/拼音：|释义：/)
            const [ head, ...other] = pinyin.match(/\S+/g)
            if (value.trim()) {
                const word = { value: value.trim(), pinyin, head: replacer(head), tail: replacer(other.pop()), desc}
                words.push(word)
                map.set(word.value, word)
                const list = map_head.get(word.head) || []
                list.push(word)
                map_head.set(word.head, list)
            }
        }
    })

    const form = document.querySelector('form')
    /** @type {HTMLDivElement} */
    const panel = document.querySelector('#console-panel')
    form.addEventListener('submit', function (e) {
        e.preventDefault()
        const begin_word = form.elements.namedItem('begin_word')['value']
        const calc_times = form.elements.namedItem('calc_times')['value'] | 0
        const word = map.get(begin_word)
        if (!word) {
            panel.classList.remove('is-link')
            panel.classList.add('is-danger')
            panel.innerText = '哟呵, 成语不存在哦骚年！'
        } else {
            panel.classList.remove('is-danger')
            panel.classList.add('is-link')
            let w = word
            let results = [w]
            for (let i = 1; i < calc_times; i++) {
                const list = map_head.get(w.tail)
                if (list && list.length) {
                    w = list[Math.floor(Math.random() * list.length)]
                    results.push(w)
                } else {
                    break;
                }
            }
            panel.innerHTML = results.map(r => `<p><b>${r.value}</b> <b>${r.pinyin}</b> <span class="is-size-7" title="${r.desc}">${r.desc.substring(0, 80)}</span></p>`).join('')
        }
    })
})();
