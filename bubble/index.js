// @ts-check

const COLORS = ['#F2B527', '#42C873', '#187DE2', '#97C842', '#FF8888', '#F29A27', '#A97EC9', '#8289A1'];
const SIZES = [100, 90, 80, 40, 30, 20, 15, 12, 10];
const SIZE = { min: 10, max: 100 };

/**
 * @type {{ name?: string, value?: number }[]}
 */
const demo_list = [
    { name: '意大利', value: 15385 },
    { name: '伊朗', value: 10075 },
    { name: '韩国', value: 7979 },
    { name: '西班牙', value: 3146 },
    { name: '法国', value: 2889 },
    { name: '德国', value: 2745 },
    { name: '美国', value: 1726 },
    { name: '瑞士', value: 868 },
];

/**
 * 
 * @param {{ data: typeof demo_list, width: number, height: number, colors: string[], sizes: number[], size: typeof SIZE }} args
 * @returns {{left: number, top: number, radius: number, color: string}[]}
 */
export const render = function ({
    data = demo_list,
    width = 600,
    height = 400,
    colors = COLORS,
    sizes = SIZES,
    size = SIZE,
}) {
    var max = data.reduce(function (max, item) { return Math.max(max, item.value); }, 0)

    var pointer = { left: width / 2, top: height / 2, radius: sizes ? sizes[0] : size.max, color: colors[0] };
    var rendered = [pointer];

    data.map(function (item, i) {
        var radius = 0;
        if (sizes) {
            radius = sizes[i]
        } else if (size) {
            radius = Math.max(size.min, item.value * size.max / max);
        }
        if (i === 1) {
            let p = randomDistancePoint(radius + pointer.radius + 10)
            rendered.push({
                left: pointer.left + p.left,
                top: pointer.top +  p.top,
                radius,
                color: colors[i % colors.length]
            })
        }

    })

    return rendered
}
/**
 * 
 * @param {number} dist
 * @returns {{left: number, top: number}}
 */
function randomDistancePoint(dist) {
    const deg = 2 * Math.PI * Math.random()
    return {
        left: Math.cos(deg) * dist / 2,
        top: Math.sin(deg) * dist / 2,
    }
}