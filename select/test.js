/**
 * @param {character[][]} board
 * @return {number}
 */
var numRookCaptures = function (board) {
    let sum = 0
    let ll = {}
    for (let i = 0; i < 8; i++) {
        let tt = '.'
        for (let j = 0; j < 8; j++) {
            let t = board[i][j]
            if (t === 'R') {
                if (ll[j] === 'p') {
                    sum++
                }
                if (tt === 'p') {
                    sum++
                }
                for (let m = i + 1; m < 8; m++) {
                    if (board[m][j] != '.') {
                        if (board[m][j] === 'p') sum++
                        break;
                    }
                }
                for (let n = j + 1; n < 8; n++) {
                    if (board[i][n] != '.') {
                        if (board[i][n] === 'p') sum++
                        break;
                    }
                }
                break;
            } else if (t != '.') {
                ll[j] = t
                tt = t
            }
        }
    }
    return sum;
};

numRookCaptures([
    [".", ".", ".", ".", ".", ".", ".", "."],
    [".", ".", ".", "p", ".", ".", ".", "."],
    [".", ".", ".", "R", ".", ".", ".", "p"],
    [".", ".", ".", ".", ".", ".", ".", "."],
    [".", ".", ".", ".", ".", ".", ".", "."],
    [".", ".", ".", "p", ".", ".", ".", "."],
    [".", ".", ".", ".", ".", ".", ".", "."],
    [".", ".", ".", ".", ".", ".", ".", "."]
])