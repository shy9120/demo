;(function () {

    interface IStudent {
        code?: string
        name?: string
        birth?: string
        clazz?: string
        gender?: string
    }

    class Student implements IStudent {
        code?: string
        name?: string
        birth?: string
        clazz?: string
        gender?: string
        constructor (student: IStudent) {
            Object.assign(this, student)
        }
        static from (form: HTMLFormElement) {
            let s = {}
            for (let ele of form.elements) {
                if (ele instanceof HTMLInputElement) {
                    switch (ele.type) {
                        case 'radio':
                            if (ele.checked) {
                                s[ele.name] = ele.value
                            }
                            break;
                        case 'date':
                            s[ele.name] = ele.value
                            break;
                        default:
                            s[ele.name] = ele.value
                    }
                }
            }
            return new Student(s)
        }
        static fill(form: HTMLFormElement, s: IStudent) {
            console.log(s)
            Object.keys(s).forEach(n => {
                const ele = form.querySelector<HTMLInputElement>(`[name="${n}"]`)
                switch (ele.type) {
                    case 'radio':
                        const radio = form.querySelector<HTMLInputElement>(`[name="${n}"][value="${s[n]}"]`);
                        if (radio) radio.checked = true
                        break;
                    default:
                        ele.value = s[n];
                }
            })
        }
        isValid () {
            return this.code && this.name && this.birth && this.clazz && this.gender;
        }
        createTableRow (index: number) {
            const row = document.createElement('tr')
            row.insertCell().innerText = this.code
            row.insertCell().innerText = this.name
            row.insertCell().innerText = new Date(this.birth).toLocaleDateString()
            row.insertCell().innerText = this.clazz
            row.insertCell().innerText = ['', '男', '女'][this.gender]
            row.insertCell().innerHTML = `
                <button class="edit" data-index="${index}">修改</button>
                <button class="del" data-index="${index}">删除</button>
            `
            return row;
        }
        toStdunt (): IStudent {
            const { code, name, birth, clazz, gender } = this
            return { code, name, birth, clazz, gender }
        }
    }

    class Store {
        KEY: string
        students: IStudent[]
        constructor(KEY = 'STORE_KEY_STUDENT') {
            this.KEY = KEY
            this.students = JSON.parse(localStorage.getItem(KEY) || '[]')
        }
        save() {
            localStorage.setItem(this.KEY, JSON.stringify(this.students))
        }
        add(s: IStudent) {
            this.students.push(s)
            this.save()
        }
        remove(index: number) {
            this.students.splice(index, 1)
            this.save()
        }
        update(index: number, s: IStudent) {
            this.students.splice(index, 1, s)
            this.save()
        }
    }

    const store = new Store()
    const form = document.querySelector('form');
    const btnSubmit = document.querySelector<HTMLInputElement>('#submit-button');
    const container = document.querySelector<HTMLTableElement>('#list-container');

    function render () {
        const { students = [] } = store
        container.innerHTML = ''
        for (let i = 0; i < students.length; i++) {
            const s = new Student(students[i]);
            container.appendChild(s.createTableRow(i))
        }
    }

    render();

    let update_index = -1;
    form.addEventListener('submit', function (e) {
        e.preventDefault();
        const student = Student.from(this)
        if (student.isValid()) {
            if (btnSubmit.value === '添加') {
                store.add(student.toStdunt())
            } else {
                store.update(update_index, student.toStdunt())
            }
            form.dispatchEvent(new CustomEvent('reset'))
            render();
        } else {
            alert('有未填写字段:\n' + JSON.stringify(student.toStdunt(), null, 2));
        }
    });
    form.addEventListener('reset', function () {
        update_index = -1;
        btnSubmit.value = '添加'
    })
    container.addEventListener('click', function (e) {
        if (e.target instanceof HTMLButtonElement) {
            const classList = e.target.classList;
            const index = Number(e.target.dataset['index'])
            if (classList.contains('del')) {
                confirm('确定删除？') && store.remove(index)
            } else if (classList.contains('edit')) {
                update_index = index;
                btnSubmit.value = '修改';
                Student.fill(form, store.students[index]);
            }
            render()
        }
    })

})();
