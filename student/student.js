;
(function () {
    var Student = (function () {
        function Student(student) {
            Object.assign(this, student);
        }
        Student.from = function (form) {
            var s = {};
            for (var _i = 0, _a = form.elements; _i < _a.length; _i++) {
                var ele = _a[_i];
                if (ele instanceof HTMLInputElement) {
                    switch (ele.type) {
                        case 'radio':
                            if (ele.checked) {
                                s[ele.name] = ele.value;
                            }
                            break;
                        case 'date':
                            s[ele.name] = ele.value;
                            break;
                        default:
                            s[ele.name] = ele.value;
                    }
                }
            }
            return new Student(s);
        };
        Student.fill = function (form, s) {
            console.log(s);
            Object.keys(s).forEach(function (n) {
                var ele = form.querySelector("[name=\"" + n + "\"]");
                switch (ele.type) {
                    case 'radio':
                        var radio = form.querySelector("[name=\"" + n + "\"][value=\"" + s[n] + "\"]");
                        if (radio)
                            radio.checked = true;
                        break;
                    default:
                        ele.value = s[n];
                }
            });
        };
        Student.prototype.isValid = function () {
            return this.code && this.name && this.birth && this.clazz && this.gender;
        };
        Student.prototype.createTableRow = function (index) {
            var row = document.createElement('tr');
            row.insertCell().innerText = this.code;
            row.insertCell().innerText = this.name;
            row.insertCell().innerText = new Date(this.birth).toLocaleDateString();
            row.insertCell().innerText = this.clazz;
            row.insertCell().innerText = ['', '男', '女'][this.gender];
            row.insertCell().innerHTML = "\n                <button class=\"edit\" data-index=\"" + index + "\">\u4FEE\u6539</button>\n                <button class=\"del\" data-index=\"" + index + "\">\u5220\u9664</button>\n            ";
            return row;
        };
        Student.prototype.toStdunt = function () {
            var _a = this, code = _a.code, name = _a.name, birth = _a.birth, clazz = _a.clazz, gender = _a.gender;
            return { code: code, name: name, birth: birth, clazz: clazz, gender: gender };
        };
        return Student;
    })();
    var Store = (function () {
        function Store(KEY) {
            if (KEY === void 0) { KEY = 'STORE_KEY_STUDENT'; }
            this.KEY = KEY;
            this.students = JSON.parse(localStorage.getItem(KEY) || '[]');
        }
        Store.prototype.save = function () {
            localStorage.setItem(this.KEY, JSON.stringify(this.students));
        };
        Store.prototype.add = function (s) {
            this.students.push(s);
            this.save();
        };
        Store.prototype.remove = function (index) {
            this.students.splice(index, 1);
            this.save();
        };
        Store.prototype.update = function (index, s) {
            this.students.splice(index, 1, s);
            this.save();
        };
        return Store;
    })();
    var store = new Store();
    var form = document.querySelector('form');
    var btnSubmit = document.querySelector('#submit-button');
    var container = document.querySelector('#list-container');
    function render() {
        var _a = store.students, students = _a === void 0 ? [] : _a;
        container.innerHTML = '';
        for (var i = 0; i < students.length; i++) {
            var s = new Student(students[i]);
            container.appendChild(s.createTableRow(i));
        }
    }
    render();
    var update_index = -1;
    form.addEventListener('submit', function (e) {
        e.preventDefault();
        var student = Student.from(this);
        if (student.isValid()) {
            if (btnSubmit.value === '添加') {
                store.add(student.toStdunt());
            }
            else {
                store.update(update_index, student.toStdunt());
            }
            form.dispatchEvent(new CustomEvent('reset'));
            render();
        }
        else {
            alert('有未填写字段:\n' + JSON.stringify(student.toStdunt(), null, 2));
        }
    });
    form.addEventListener('reset', function () {
        update_index = -1;
        btnSubmit.value = '添加';
    });
    container.addEventListener('click', function (e) {
        if (e.target instanceof HTMLButtonElement) {
            var classList = e.target.classList;
            var index = Number(e.target.dataset['index']);
            if (classList.contains('del')) {
                confirm('确定删除？') && store.remove(index);
            }
            else if (classList.contains('edit')) {
                update_index = index;
                btnSubmit.value = '修改';
                Student.fill(form, store.students[index]);
            }
            render();
        }
    });
})();
