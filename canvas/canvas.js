// @ts-check
(function () {

    /**
     * @param {ImageData} imgData
     */
    var render = function (imgData) {
        var { width, height, data } = imgData
        var txt = '后浪'
        var PIX = 10
        var app = document.querySelector('canvas')
        app.width = width * PIX * txt.length
        app.height = height * PIX
        var ctx = app.getContext('2d')
        for (let i = 0; i < height; i += 1 ) {
            for (let j = 0; j < width; j += txt.length) {
                let n = i * width * 4 + j * 4
                ctx.strokeStyle = `rgba(${data[n]}, ${data[n + 1]}, ${data[n + 2]}, ${data[n + 3]})`
                ctx.font = `${PIX}px serif`
                ctx.strokeText(txt, j * PIX, i * PIX)
            }
        }
    }
    var initImage = function () {
        var app = document.createElement('canvas')
        var image = new Image()
        image.addEventListener('load', function () {
            app.width = image.width
            app.height = image.height
            var ctx = app.getContext('2d')
            ctx.beginPath();
            ctx.drawImage(image, 0, 0, app.width, app.height)
            render(ctx.getImageData(0, 0, app.width, app.height))
        })
        image.src = `./timg.png`
    }

    initImage()
})();
