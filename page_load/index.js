define(["require", "exports", "react", "react-dom"], function (require, exports, React, ReactDOM) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    const _console = document.getElementById('console');
    const console = {
        log: (info) => _console.innerText = info
    };
    const pageSize = 10;
    const App = () => {
        const [page, setPage] = React.useState(1);
        const [total, setTotal] = React.useState(0);
        const [list, setData] = React.useState([]);
        const [cates, setCates] = React.useState([]);
        const [curr, setCurr] = React.useState(0);
        const app_ref = React.useRef();
        const loadPageInfo = async () => {
            if (list.length > page * pageSize) {
                // 表示当前页数据已经加载完了， 啥也不干
                return;
            }
            if (total && total === list.length) {
                console.log('到头了,别拉了！');
                return;
            }
            console.log('加载中...');
            const res = await fetch(`./data/page0${page}.json`).then(res => new Promise(reso => setTimeout(() => {
                res.json().then(reso); // 故意拉长时间调试使用
            }, 1000)));
            const data = list.concat(res.data);
            setData(data);
            setTotal(res.total);
            console.log(`加载完成,  ${data.length}/${res.total} `);
        };
        const onChangeCate = (index) => {
            setCurr(index);
            setData([]);
            setTotal(0);
            setPage(1);
        };
        React.useEffect(function () {
            const cate = cates[curr];
            if (cate) {
                loadPageInfo();
            }
        }, [cates, curr, page]);
        React.useEffect(function () {
            fetch(`./data/cate.json`).then(res => res.json()).then(cates => setCates(cates));
        }, []);
        setInterval(function () {
            if (app_ref.current) {
                const _app = app_ref.current;
                const p = Math.ceil((_app.scrollTop + _app.clientHeight + 50) / (150 * pageSize));
                if (p != page) {
                    setPage(p);
                }
            }
        }, 100);
        return React.createElement(React.Fragment, null,
            React.createElement("div", { className: "cate" }, cates.map((cate, i) => React.createElement("a", { className: curr === i ? 'current' : '', onClick: () => onChangeCate(i) }, cate.name))),
            React.createElement("div", { className: "inner", ref: app_ref },
                list.map((user, i) => React.createElement("div", { className: "user-card", key: i },
                    React.createElement("p", null,
                        i + 1,
                        ". \u59D3\u540D: ",
                        user.name,
                        ", \u5E74\u9F84: ",
                        user.age),
                    React.createElement("p", null,
                        "\u804C\u4E1A: ",
                        user.occ))),
                !!total && total === list.length && React.createElement("p", { className: "ender" }, "\u6728\u7684\u5566\uFF01")));
    };
    ReactDOM.render(React.createElement(App, null), document.getElementById('app'));
});
// tsc -t ESNEXT -m amd --jsx 'react' index.tsx
