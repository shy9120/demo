(function () {
    const container = document.querySelector('.cube-container')
    const inner = container.querySelector('.cube-inner')

    const toggleClass = () => {
        if (container.classList.contains('gather')) {
            inner.style.transform = getComputedStyle(inner).transform;
            inner.style.animation = 'unset';
            setTimeout(function() {
                inner.style.transform = null
                inner.style.animation = null
                container.classList.remove('gather')
            }, 100);
        } else {
            container.classList.add('gather')
        }
    }
    const setSize = function () {
        const { width, height } = container.getBoundingClientRect()
        inner.style.width = width + 'px'
        inner.style.height = height + 'px'
    }
    setSize()
    addEventListener('resize', setSize)

    const toggle = document.querySelector('.cube-toggle')
    if (toggle) {
        toggle.addEventListener('click', toggleClass)
    }
})();